#include <Arduino.h>
#include<PIMA_Protocol.h>

// Definition of pins used for serial communication to tranceiver data.
#define RX_PIN 0
#define TX_PIN 1

// Defining data field sizes.
#define STX_SIZE 1
#define DATA_SIZE_SIZE 1
#define COMMAND_SIZE 1
#define ETX_SIZE 1
#define CRC_SIZE 2

// Definition of the maximum size of data to be transmitted.
#define MAX_DATA_SIZE 255

void setup() {
  // Serial communication initialization
  Serial.begin(2400);
}

void loop() {
  // Example of data to be transmitted.
  byte command = 0x01;
  byte data[MAX_DATA_SIZE] = {0x01, 0x23, 0x45, 0x67, 0x89};
  byte data_size = sizeof(data);
  unsigned int crc = calculateCRC(command, data, data_size);

  //Mounting the Data Pack with model of Protocol PIMA.
  byte packet[STX_SIZE + DATA_SIZE_SIZE + COMMAND_SIZE + data_size + ETX_SIZE + CRC_SIZE];
  packet[0] = 0x02; // STX
  packet[1] = data_size + COMMAND_SIZE; // DATA SIZE
  packet[2] = command; // COMMAND
  memcpy(packet + STX_SIZE + DATA_SIZE_SIZE + COMMAND_SIZE, data, data_size);
  packet[STX_SIZE + DATA_SIZE_SIZE + COMMAND_SIZE + data_size] = 0x03; // ETX
  packet[STX_SIZE + DATA_SIZE_SIZE + COMMAND_SIZE + data_size + ETX_SIZE] = crc >> 8;
  packet[STX_SIZE + DATA_SIZE_SIZE + COMMAND_SIZE + data_size + ETX_SIZE + 1] = crc & 0xFF;

  //Sending data via serial communication.
  Serial.write(packet, sizeof(packet));
  delay(1000);
}
  // CRC calculation of the data.
unsigned int calculateCRC(byte command, byte *data, byte length) {
  unsigned int crc = 0;
  crc ^= command;
  for (byte i = 0; i < length; i++) {
    crc ^= (unsigned int)data[i] << 8;
    for (byte j = 0; j < 8; j++) {
      if (crc & 0x8000) {
        crc = (crc << 1) ^ 0x1021;
      } else {
        crc <<= 1;
      }
    }
  }
  return crc;
}