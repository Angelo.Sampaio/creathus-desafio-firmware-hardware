#include <Arduino.h> 
#include <SerialReadWrite_PIMA.h>

#define PREAMBLE_SIZE 2
#define IDENTIFIER_SIZE 5
#define TOTAL_MSG_SIZE 1
#define SCOPE_INDEX_SIZE 2
#define CRC_SIZE 2
#define MAX_DATA_SIZE 1024

typedef struct {
  uint8_t preamble[PREAMBLE_SIZE];
  uint8_t identifier[IDENTIFIER_SIZE];
  uint8_t max[TOTAL_MSG_SIZE];
  uint8_t scope_index[SCOPE_INDEX_SIZE];
  uint8_t data[MAX_DATA_SIZE];
  uint16_t crc;
} Package;

void receive_package(uint8_t *buffer, uint32_t max_size) {
  uint32_t position = 0;
  while (position < max_size) {
    uint8_t data = Serial.read();
    if (data != -1) {
      buffer[position++] = data;
    }
  }
}

uint16_t calc_crc(Package *package) {
  uint16_t crc = 0xFFFF;
  for (uint32_t i = 0; i < PREAMBLE_SIZE + IDENTIFIER_SIZE + TOTAL_MSG_SIZE + SCOPE_INDEX_SIZE + package->max[0]; i++) {
    crc ^= package->data[i];
    for (uint8_t j = 0; j < 8; j++) {
      if (crc & 0x0001) {
        crc = (crc >> 1) ^ 0xA001;
      } else {
        crc >>= 1;
      }
    }
  }
  return crc;
}

//RECEIVE SCOPE OF PROTOCOL PIMA AND BUFFER OVERFLOW MEMORY TREATMENT
void receive_package(Package *package) {
  uint8_t buffer[PREAMBLE_SIZE + IDENTIFIER_SIZE + TOTAL_MSG_SIZE + SCOPE_INDEX_SIZE + MAX_DATA_SIZE + CRC_SIZE];
  receive_package(buffer, PREAMBLE_SIZE + IDENTIFIER_SIZE + TOTAL_MSG_SIZE + SCOPE_INDEX_SIZE);
  memcpy(package->preamble, buffer, PREAMBLE_SIZE);
  memcpy(package->identifier, buffer + PREAMBLE_SIZE, IDENTIFIER_SIZE);
  memcpy(package->max, buffer + PREAMBLE_SIZE + IDENTIFIER_SIZE, TOTAL_MSG_SIZE);
  memcpy(package->scope_index, buffer + PREAMBLE_SIZE + IDENTIFIER_SIZE + TOTAL_MSG_SIZE, SCOPE_INDEX_SIZE);
  uint8_t data_max = package->max[0];
  if (data_max > MAX_DATA_SIZE) {
    data_max = MAX_DATA_SIZE;
  }
  receive_package(package->data, data_max);
  receive_package((uint8_t *)&package->crc, CRC_SIZE);
  uint16_t crc_calculate = calc_crc(package);
  if (crc_calculate != package->crc) {
    // CRC invalidate, to thwro away data.
  }
}

//STATE MACHINE CONTROL LIST
typedef enum {
  STATE_INITIAL,
  RECEIVE_STATE_PREAMBLE,
  RECEIVE_STATE_IDENTIFIER,
  RECEIVE_STATE_MAX,
  RECEIVE_STATE_SCOPE_INDEX,
  RECEIVE_STATE_DATA,
  RECEIVE_STATE_CRC,
  VALIDATING_STATE_CRC,
  PACKAGE_STATE_VALID,
  PACKAGE_STATE_INVALID,
  ERROR_STATE
} State;

State current_state = STATE_INITIAL;
Package package;

void reset_package() {
  memset(&package, 0, sizeof(Package));
}

void reset_state() {
  current_state = STATE_INITIAL;
  reset_package();
}
//Process state mensage on serial badwith
void process_byte(uint8_t byte) {
  switch (current_state) {
    case STATE_INITIAL:
      if (byte == 0xAA) {
        package.preamble[0] = byte;
        current_state = RECEIVE_STATE_PREAMBLE;
      } else {
        reset_state();
      }
      break;
    case RECEIVE_STATE_PREAMBLE:
      if (byte == 0x55) {
        package.preamble[1] = byte;
        current_state = RECEIVE_STATE_IDENTIFIER;
      } else {
        reset_state();
      }
      break;
    case RECEIVE_STATE_IDENTIFIER:
      memcpy(package.identifier, &byte, sizeof(byte));
      current_state = RECEIVE_STATE_MAX;
      break;
    case RECEIVE_STATE_MAX:
      memcpy(package.max, &byte, sizeof(byte));
      current_state = RECEIVE_STATE_SCOPE_INDEX;
      break;
    case RECEIVE_STATE_SCOPE_INDEX:
      memcpy(package.scope_index, &byte, sizeof(byte));
      current_state = RECEIVE_STATE_DATA;
      break;
    case RECEIVE_STATE_DATA:
      if (package.max[0] < MAX_DATA_SIZE) {
        package.data[package.max[0]] = byte;
        package.max[0]++;
      } else {
        reset_state();
      }
      if (package.max[0] == package.max[1]) {
        current_state = RECEIVE_STATE_CRC;
      }
      break;
    case RECEIVE_STATE_CRC:
      memcpy(&package.crc, &byte, sizeof(byte));
      current_state = VALIDATING_STATE_CRC;
      break;
    case VALIDATING_STATE_CRC:
      if (calc_crc(&package) == package.crc) {
        current_state = PACKAGE_STATE_VALID;
      } else {
        current_state = PACKAGE_STATE_INVALID;
      }
      break;
    case PACKAGE_STATE_VALID: 
      reset_state();
      break;
    case PACKAGE_STATE_INVALID: 
      reset_state();
      break;
    case ERROR_STATE: 
      reset_state();
      break;
  }
}

void setup() {
  Serial.begin(2400);   
}

void loop() { 
  Package package;
  receive_package(&package);
  Serial.println("Wait Package PIMA on Serial...");
  delay(5000);
  }