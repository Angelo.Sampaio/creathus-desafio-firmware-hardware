#include <LiquidCrystal.h> 
#include "EmonLib.h"        

//set and calibration variable of calc rms. 
EnergyMonitor emon1;   
#define pin_sct A0     
#define effective_voltage 1480
#define calibration_offset 6.0601
#define pin_relay 8   

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);  //set pin's led lcd16x2

//Set mains voltage
int eletric_power = 127;
int power = 0;
void setup() {

  lcd.begin(16, 2);
  lcd.clear();
  Serial.begin(9600);
  pinMode(pin_relay, OUTPUT);
    digitalWrite(pin_relay, HIGH);
  emon1.current(pin_sct, calibration_offset);
  //Set informations initials on display
    lcd.setCursor(0, 0);
    lcd.print("Corr.(A):");
    lcd.setCursor(0, 1);
    lcd.print("Pot. (W):");
}

void loop() {
  //Calculates the current effect.ve
  double Irms = emon1.calcIrms(effective_voltage);
  double power = Irms * eletric_power;
  //Show the current value
  Serial.print("Corrente : ");
  Serial.print(Irms);  // Irms
  lcd.setCursor(10, 0);
  lcd.print(Irms);

  //Calculates and displays the power value
  Serial.print(" Potencia : ");
  Serial.println(power);
  lcd.setCursor(10, 1);
  lcd.print("      ");
  lcd.setCursor(10, 1);
  if (Irms < 0.04  ) {
     lcd.print(0, 1); 
  }else{ 
    lcd.print(power, 1);
  } 
  delay(1000);
}