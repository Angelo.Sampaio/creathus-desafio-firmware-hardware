#include "SerialRead.h"

SerialRead::SerialRead() {
}

void SerialRead::begin(int baudrate) {
  _baudrate = baudrate;
  Serial.begin(_baudrate);
}

void SerialRead::readData() {
  if (Serial.available()) {
    int data = Serial.read();
    Serial.println(data);
  }
}