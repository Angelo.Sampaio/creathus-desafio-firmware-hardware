#ifndef SerialRead_h
#define SerialRead_h

#include "Arduino.h"

class SerialRead {
  public:
    SerialRead();
    void begin(int baudrate);
    void readData();
  private:
    int _baudrate;
};

#endif
